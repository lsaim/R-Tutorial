This is a repository for a simple R-tutorial focused on administrative work at UW-Madison. The tutorial text can be found here:

https://lsaim.pages.doit.wisc.edu/R-Tutorial/Text/Tutorial_Text.html

For a tutorial that includes more contect about base-r see the old tutorial:

https://lsaim.pages.doit.wisc.edu/R-Tutorial/Archive/R%20Introduction/SimpleIntroduction.html



