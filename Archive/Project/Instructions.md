# R Project Instructions

In this project we will be exploring data about political freedom and economic/wellbeing measures. You will integrate data from two different sources and show patterns using graphs in a .qmd markdown report.  I have a few questions that can guide your explorations, but the basic idea is to have a bit of fun with R. Try to figure some things out that interest you about the data as well.

Let's aim to have the project done by Friday January 13th, 2023. Everyone will have their own folder in this gitlab repository to upload the documents. This will allow you to upload your own .qmd and rendered .html document using git commands.  

Note that all of the following actions can be accomplished by cloning (or pulling if already cloned) this github repository.

First download the Freedom House data set (or synch this repository)  
- [Freedom House Ratings](https://git.doit.wisc.edu/lsaim/R-Tutorial/-/blob/main/Project/FreedomHouseData.csv)

I've included some code to get you started in the following .qmd file.  But you also have a .qmd file to edit located in your individual folder.  This is called a quarto file, and when you hit the "render" button at the top of the screen, it will run all R code and output graphs into an html format. Try it out before editing it. By default it also outputs the code you used to process data and create plots, but you can turn that off by through settings in the code "chunk." Note that in the first code chunk I turn on code output and in the second code chunk I turn it off. Look at the top of the code chunk to see the difference.

- [Project Quarto File](https://git.doit.wisc.edu/lsaim/R-Tutorial/-/blob/main/Project/Freedom_Econ_Project.qmd)


Once you have the data loaded, write a report in the .qmd file addressing the following.

1. Characterize the two data sets. When do the data begin, and when do they end?  What are the measures provided in each data set and what are the categories (year, country, continent)?  What are the ranges of these measures?  Are there any NAs?  Are there any difficulties merging the data? In particular, is there some data in 

2. Provid more detailed summary statistics (min, mean, max) of per-captia GDP and life expectancy by continent and country.  Put this data into a table.  Produce two line plots, both with year on the x-axis and line color representing continent: one graph with per-capita GDP on the y-axis and the other with life expectancy on the y-axis.

3. Produce a stacked bar graph showing the percentage of countries in each Freedom House status, with year along the x-axis and color representing the status. Make sure the legend displays understandable labels for the statuses. They should be as follows:
    - NF: Not Free
    - PF: Partially Free
    - F: Free

4. Merge the Economic/life expectancy data with the Freedom House data.  Produce four scatter plots: per-capita GDP against the civil liberties and political liberties scores, and life expectancy against those scores. Also find correlations. Are any correlations notable? If you are feeling ambitious, plot regression lines on the scatter plots. What is the adjusted R^2?

5. Make a bar graph showing the mean per-capita gdp (on the top facet) and life expectancy (on the bottom facet), by Freedom House status.
