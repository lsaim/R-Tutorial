##Summary Problem Set


##Preliminary
#1) Read in CourseGrades.csv, SemesterData.csv, and StudentData.csv. 
#For simplicity, name them after the file (excluding ".csv").
#These provide credit and grade data for a single course (let's call it PHIL101),
# along with information about the students who took that course.

getwd()
setwd("C:/Users/zenz/Documents/R-Tutorial")
CourseGrades <-  read.csv("Homework/CourseGrades.csv")
SemesterData <-  read.csv("Homework/SemesterData.csv")
StudentData <-  read.csv("Homework/StudentData.csv")

#change variables to character strings
library(stringr)

CourseGrades$STU_NUM <- str_pad(string = as.character(CourseGrades$STU_NUM), width = 6, 
                                side = c("left"), pad = "0")
SemesterData$STU_NUM <-  str_pad(string = as.character(SemesterData$STU_NUM), width = 6, 
                                 side = c("left"), pad = "0")
StudentData$STU_NUM <-  str_pad(string = as.character(StudentData$STU_NUM), width = 6, 
                                side = "left", pad = "0")

CourseGrades$SEMESTER <- as.character(CourseGrades$SEMESTER)
SemesterData$SEMESTER <- as.character(SemesterData$SEMESTER)


#2) Are there any students and/or terms in StudentData or 
#SemesterData that are not found in CourseGrades? If so,
#provide a lists of any.  Are any students in CourseGrades not
# found in either of the other data frames?

STU_StudentData_NoCourseData <-  unique(StudentData[which(!(StudentData$STU_NUM %in% 
                                                              CourseGrades$STU_NUM)),]$STU_NUM)
#StudentData[which(!(StudentData$STU_NUM %in% CourseGrades$STU_NUM)),c("STU_NUM")]
STU_SemesterData_NoCourseData <-  unique(SemesterData[which(!(SemesterData$STU_NUM %in% 
                                                                CourseGrades$STU_NUM)),]$STU_NUM)




##equiv
#IDs_course <- unique(CourseGrades$STU_NUM)
#IDs_student <- unique(StudentData$STU_NUM)
#students.not.course.IDs <- IDs_student[which(!(IDs_student %in% IDs_course))]

##equiv
#setdiff(StudentData$STU_NUM, CourseGrades$STU_NUM)



#3) Merge CourseGrades with StudentData and SemesterData, so that you have a data frame that lists
#information about the student and the semsester when the student took the course. Make sure 
#that you keep all data in CourseGrades, but don't include data from the other data frames that doesn't
#merge with CourseGrades.

grade.data <- merge(x = CourseGrades, y = StudentData, 
                    by = c("STU_NUM"), all.x = TRUE)
grade.data <- merge(x = grade.data, y = SemesterData,
                     by = c("STU_NUM", "SEMESTER"), 
                    all.x = TRUE)

##if columns are different names
#StudentData.ALT <- StudentData
#colnames(StudentData.ALT)[1] <- "ID"

#grade.data <- merge(x = CourseGrades, y = StudentData.ALT, 
#                    by.x = c("STU_NUM"),by.y = c("ID"), all.x = TRUE)


##Cleaning the Data
#4) Determine what types of grades are found in the CourseGrades data. 
#Hint: You can do this either with unique() or table(). Try both! 
#Next clean the grade data, eliminating all grade data that isn't in this list:
#A,AB,B,BC,C,D,F.

unique(grade.data$OFFICIAL_GRADE)
sort(unique(grade.data$OFFICIAL_GRADE))
table(grade.data$OFFICIAL_GRADE)
table(grade.data$OFFICIAL_GRADE,grade.data$CREDITS_TAKEN)

Odd_grades <- grade.data[which(grade.data$OFFICIAL_GRADE %in% c("", "NW", "S", "U", "W")),]

grade.data <- grade.data[which(grade.data$OFFICIAL_GRADE %in% c("A", "AB", "B", "BC", "C", "D", "F")),]




#5) Check Cumulative_GPA, VERBAL scores, and QUANT scores for missing data. 
#How many NAs exist in the merged CourseGrades data for these three columns?


summary(grade.data)

grade.data.no_quant_na <- grade.data[which(!(is.na(grade.data$QUANT))),]


grade.data[which(is.na(grade.data$Cumulative_GPA)),]
(count_gpa_na <- sum(is.na(grade.data$Cumulative_GPA)))




#6)Recode UNDERREP, so that non-underrep students are coded as "N" instead of " ".


unique(grade.data$UNDERREP)
grade.data[which(grade.data$UNDERREP == ""),]$UNDERREP <- "N"
table(grade.data$UNDERREP)


#Same result as above
#grade.semester.new$UNDERREP <- ifelse(grade.semester.new$UNDERREP == "Y", "Y", "N")


#Do underrep students have more NAs for verbal?
grade.data$VERBAL_NA <- FALSE
grade.data[which(is.na(grade.data$VERBAL)),]$VERBAL_NA <- TRUE

table(grade.data$UNDERREP, grade.data$VERBAL_NA)
prop.table(table(grade.data$UNDERREP, grade.data$VERBAL_NA), margin = 1)




##Describe the Data
#7) Create a new variable that assigns numerical values to the course grades. 
#A = 4, AB = 3.5, B = 3, BC = 2.5, C = 2, D = 1, F = 0. 
#Also create a new variable called B_or_Better that is "Y" for all course grades of B or better,
# and "N" for other grades.

grade.data$Grade_num <- NA
grade.data[which(grade.data$OFFICIAL_GRADE == "A"),]$Grade_num <- 4
grade.data[which(grade.data$OFFICIAL_GRADE == "AB"),]$Grade_num <- 3.5
grade.data[which(grade.data$OFFICIAL_GRADE == "B"),]$Grade_num <- 3
grade.data[which(grade.data$OFFICIAL_GRADE == "BC"),]$Grade_num <- 2.5
grade.data[which(grade.data$OFFICIAL_GRADE == "C"),]$Grade_num <- 2
grade.data[which(grade.data$OFFICIAL_GRADE == "D"),]$Grade_num <- 1
grade.data[which(grade.data$OFFICIAL_GRADE == "F"),]$Grade_num <- 0


table(grade.data$OFFICIAL_GRADE, grade.data$Grade_num)

grade.data$B_or_Better <- "N"
grade.data[which(grade.data$Grade_num >= 3),]$B_or_Better <- "Y"

##alternative
#grade.data$B_or_Better <- ifelse(grade.data$Grade_num >= 3, 
#                                 "Y", "N")
  
table(grade.data$OFFICIAL_GRADE, grade.data$B_or_Better)

#8) Describe the grade data by GENDER and UNDERREP to see if there are any systematic differences.
# Make a tables of OFFICIAL_GRADE and B_or_Better by GENDER and UNDERREP. 
# Use prop.table to figure out whether the proportion of students getting each grade/B_or_Better 
# differs between groups.

table(grade.data$GENDER, grade.data$UNDERREP,  grade.data$OFFICIAL_GRADE)
table(grade.data$UNDERREP, grade.data$OFFICIAL_GRADE)

prop.table(table(grade.data$GENDER, grade.data$OFFICIAL_GRADE), margin = 1)
prop.table(table(grade.data$UNDERREP, grade.data$OFFICIAL_GRADE), margin = 1)



#9) In order to describe grade data based upon the academic variables, 
#it helps to recode them as categorical variables. 
#Find the median QUANT and Cumulative_GPA in the data set and then create columns 
#that indicate whether the student is >= the median or < the median.
#You'll probably need to deal with NAs.
#Next, using these variables, create tables and prop.tables like in (8).

STU_QUANT <- unique(grade.data[,c("STU_NUM", "QUANT")]) 
STU_GPA <- unique(grade.data[,c("STU_NUM","SEMESTER", "Cumulative_GPA")])

#Method 1 (longest)
#STU_QUANT_NALookup <- STU_QUANT[which(is.na(STU_QUANT$QUANT)),]$STU_NUM
#STU_QUANT <- STU_QUANT[which(!(STU_QUANT$STU_NUM %in% STU_QUANT_NALookup)),]
#MEDIAN_QUANT <-  median(STU_QUANT$QUANT)


#Method 2 (a bit shorter)
#STU_QUANT <- STU_QUANT[which(!(is.na(STU_QUANT$QUANT))),]
#MEDIAN_QUANT <-  median(STU_QUANT$QUANT)

#MEDIAN_QUANT <- median(STU_QUANT$QUANT)

#Method 3 (shortest)
MEDIAN_QUANT <- median(STU_QUANT$QUANT, na.rm = TRUE)


#Find one median for all terms, with latest term
STU_LATEST_TERM <- aggregate(grade.data$SEMESTER,
                             by = list(grade.data$STU_NUM),
                             FUN = max)
colnames(STU_LATEST_TERM) <- c("STU_NUM", "SEMESTER")
grade.data.LATEST_TERM <- merge(x = grade.data,
                                y = STU_LATEST_TERM,
                                by = c("STU_NUM", "SEMESTER"))

MEDIAN_GPA <- median(grade.data$Cumulative_GPA)



grade.data$Greater_Than_Equal_To_Median_GPA <- FALSE
grade.data[which(grade.data$Cumulative_GPA >= MEDIAN_GPA),]$Greater_Than_Equal_To_Median_GPA <- TRUE

#Alternative to find one median GPA per semester
#MEDIAN_GPA_TERM <- aggregate(grade.data$Cumulative_GPA, 
#                             by = list(grade.data$SEMESTER), 
#                             FUN = median)
#colnames(MEDIAN_GPA_TERM) <- c("SEMESTER", "MEDIAN_GPA")


grade.data$Greater_Than_Equal_To_Median_QUANT <- FALSE
grade.data[which(grade.data$QUANT >= MEDIAN_QUANT),]$Greater_Than_Equal_To_Median_QUANT <- TRUE



##BONUS!! Statistics
#10) You can find the correlation between two columns (x and y) using the cor(x,y) function.
#Find the correlations between the numerical value of grade and Cumulative_GPA as well as QUANT.
#You will need to manage NAs. Remember that correlation ranges from 0 (no correlation) 
#and +-1 (perfect correlation).

cor(grade.data$Grade_num, grade.data$Cumulative_GPA)
cor(grade.data$Grade_num, grade.data$QUANT, use = "complete.obs")
cor(grade.data$Grade_num, grade.data$VERBAL, use = "complete.obs")




#Congrats, you're doing statistics!

